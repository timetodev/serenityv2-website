<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190716154351 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE vehicle_categories');
        $this->addSql('DROP TABLE vehicle_sold');
        $this->addSql('DROP TABLE vehicles');
        $this->addSql('DROP TABLE vehicles_for_sale');
        $this->addSql('DROP TABLE weekly_run');
        $this->addSql('ALTER TABLE email ADD identifier VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE users (identifier VARCHAR(50) NOT NULL COLLATE utf8mb4_bin, license VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_bin, money INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_bin, skin LONGTEXT DEFAULT NULL COLLATE utf8mb4_bin, job VARCHAR(255) DEFAULT \'unemployed\' COLLATE utf8mb4_bin, job_grade INT DEFAULT NULL, loadout LONGTEXT DEFAULT NULL COLLATE utf8mb4_bin, position VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_bin, bank INT DEFAULT NULL, permission_level INT DEFAULT NULL, `group` VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_bin, firstname VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_bin, lastname VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_bin, dateofbirth VARCHAR(25) DEFAULT NULL COLLATE utf8mb4_bin, sex VARCHAR(10) DEFAULT NULL COLLATE utf8mb4_bin, height VARCHAR(5) DEFAULT NULL COLLATE utf8mb4_bin, phone_number VARCHAR(10) DEFAULT NULL COLLATE utf8mb4_bin, status LONGTEXT DEFAULT NULL COLLATE utf8mb4_bin, animal VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_bin, last_property VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_bin, jail INT NOT NULL, UNIQUE INDEX identifier (identifier), PRIMARY KEY(identifier)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE vehicle_categories (name VARCHAR(60) NOT NULL COLLATE latin1_swedish_ci, label VARCHAR(60) NOT NULL COLLATE latin1_swedish_ci, PRIMARY KEY(name)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE vehicle_sold (plate VARCHAR(50) NOT NULL COLLATE latin1_swedish_ci, client VARCHAR(50) NOT NULL COLLATE latin1_swedish_ci, model VARCHAR(50) NOT NULL COLLATE latin1_swedish_ci, soldby VARCHAR(50) NOT NULL COLLATE latin1_swedish_ci, date VARCHAR(50) NOT NULL COLLATE latin1_swedish_ci, PRIMARY KEY(plate)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE vehicles (model VARCHAR(60) NOT NULL COLLATE latin1_swedish_ci, name VARCHAR(60) NOT NULL COLLATE latin1_swedish_ci, price INT NOT NULL, category VARCHAR(60) DEFAULT NULL COLLATE latin1_swedish_ci, PRIMARY KEY(model)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE vehicles_for_sale (id INT AUTO_INCREMENT NOT NULL, seller VARCHAR(50) NOT NULL COLLATE utf8_general_ci, vehicleProps LONGTEXT NOT NULL COLLATE utf8_general_ci, price INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE weekly_run (id INT AUTO_INCREMENT NOT NULL, company VARCHAR(255) NOT NULL COLLATE utf8mb4_bin, start_date INT NOT NULL, harvest INT NOT NULL, sell INT NOT NULL, malus INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE email DROP identifier');
    }
}
