<?php

namespace App\Controller;

use App\Entity\Email;
use App\Entity\Users;
use App\Entity\SanctionJ;
use App\Entity\CompteEmail;
use App\Entity\ExtensionMail;
use App\Entity\OwnedVehicles;
use App\Form\CompteEmailType;
use App\Repository\EmailRepository;
use App\Repository\UsersRepository;
use App\Repository\CompteEmailRepository;
use App\Repository\ExtensionMailRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProfilController extends AbstractController
{
    /**
     * @Route("/profil", name="profil")
     */
    public function profil(UserInterface $mabite, CompteEmailRepository $repos,  ObjectManager $manager)
    {
        

        $sanction = new SanctionJ();
        $sanction = $manager->getRepository(SanctionJ::class)->findBysteam($mabite->getSteamHex());


        $compte = new CompteEmail();
        $compte = $repos->findOneBy(['steamHex' => $mabite->getSteamHex()]);

        return $this->render('profil/profil.html.twig',[
            'sanction' => $sanction,
            'emailtcheck' => $compte,
        ]);
    }

    // /**
    //  * @Route("/email", name="boite_email")
    //  */
    // public function boiteemail(UserInterface $user, EmailRepository $reposs, CompteEmailRepository $repos,  ObjectManager $manager)
    // {
    //     // Entité des compte email
    //     $compteE = new CompteEmail();
    //     $compteE = $repos->findOneBy(['steamHex' => $user->getSteamHex()]);
    //     dump($compteE);

    //     $compte = new CompteEmail();
    //     $compte = $repos->findOneBy(['steamHex' => $user->getSteamHex()]);



    //     // entité des email envoyé et reçu
    //     $email = new Email();
    //     $email = $reposs->findBy(['cc' => $compteE->getAdresseMail()]);
    //     dump($email);


        
    //     return $this->render('profil/receptionEmail.html.twig',[
    //         'emailR' => $email,
    //         'emailtcheck' => $compte,
    //     ]);
    // }

      /**
     * @Route("/emailCreate", name="add_email")
     */
    public function addEmail(Request $request, UserInterface $user, ExtensionMailRepository $repos, CompteEmailRepository $repoE,  ObjectManager $manager)
    {


        $add = new CompteEmail();
        $form = $this->createForm(CompteEmailType::class, $add);

        $compteE = new CompteEmail();
        $compteE = $repoE->findOneBy(['steamHex' => $user->getSteamHex()]);

        $form->handleRequest($request);

       
            if($form->isSubmitted() && $form->isValid()){

                
                    $posts = $request->request->all();
                    $post = $request->request->get("extension");
                 
                $add->setAdresseMail($add->getAdresseMail().''.$post);
                $manager->persist($add);
                $manager->flush();
                return $this->redirectToRoute('profil');
            }
      
        
        

        return $this->render('profil/index.html.twig',[
            'form' => $form->createView(),
            'info' => $info,
            'emailtcheck' => $compteE,
        ]);
    }

    // /**
    //  * @Route("/email/{id}", name="email_view")
    //  */
    // public function newsPerso(EmailRepository $repo, CompteEmailRepository $repoE, UserInterface $user, $id)
    // {
    //    $emailR = new Email();
    //    $emailR = $repo->find($id);

    //    $compteE = new CompteEmail();
    //    $compteE = $repoE->findOneBy(['steamHex' => $user->getSteamHex()]);

    //     return $this->render('profil/EmailView.html.twig', [
    //         'recep' => $emailR,
    //         'emailtcheck' => $compteE,
    //     ]);
    // }
}
