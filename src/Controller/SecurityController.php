<?php

namespace App\Controller;

use App\Entity\User;
use DiscordWebhooks\Embed;
use DiscordWebhooks\Client;
use App\Form\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="security_registation")
     */
    public function registration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $roles = ['ROLE_USER'];
            // discord webhooks
            $webhook = new Client('https://discordapp.com/api/webhooks/601178663026950194/7SlihX_r438oIhuQG5Rsza-LjVu2eQuheFYkixyht2_Rnw7JfDf60kIPReGHnEPP6bMS');
            $embed = new Embed();
            $value =  ' Adresse email ' .$user->getEmail(). ' SteamHex ' .$user->getSteamHex().'';
            $embed->description($value);
            $webhook->username('Register Log')->message('Nouvelle Inscription ')->embed($embed)->send();

            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $user->setRoles($roles);
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('security_login');
        }

        return $this->render('security/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/connexion", name="security_login")
     */
    public function login(){
   
        return $this->render('security/login.html.twig');
    }

     /**
     * @Route("/logout", name="security_logout")
     */
    public function logout(){}
}
